package lib

import (
	"bytes"
	"fmt"
	"github.com/gliderlabs/ssh"
	"io"
	"log"
	"os/exec"
	"runtime"
)

type CmdLine struct {
	w    io.WriteCloser
	data []byte
	history [][]byte
}

func NewCmdLine(w io.WriteCloser) *CmdLine {
	return &CmdLine{
		w: w,
	}
}

func (cmdline *CmdLine) Write(p []byte) (int, error) {
	pLen := len(p)
	for _, char := range p {
		if char == byte(127) {
			if len(cmdline.data) > 0 {
				cmdline.data = cmdline.data[0 : len(cmdline.data)-1]
				p = bytes.Replace(p, []byte{127}, []byte{8,32,8}, -1)
			} else {
				println("bell")
				p = bytes.Replace(p, []byte{127}, []byte{7}, -1)
			}
		} else if char == byte(13) {
			cmdline.history = append(cmdline.history, cmdline.data)
			cmdline.data = []byte{}
		} else {
			cmdline.data = append(cmdline.data, char)
		}
	}
	cmdline.w.Write(p)
	return pLen, nil
}

func (cmdline *CmdLine) Read(p []byte) (int, error) {
	for i, b := range cmdline.data {
		p[i] = b
	}
	return len(cmdline.data), nil
}

func RunSSHServer(port int) {
	ssh.Handle(func(s ssh.Session) {
		var cmd *exec.Cmd

		if runtime.GOOS == "windows" {
			cmd = exec.Command("powershell")
		} else {
			cmd = exec.Command("bash", "-i")
		}

		stdin, _ := cmd.StdinPipe()
		stdout, _ := cmd.StdoutPipe()
		stderr, _ := cmd.StderrPipe()

		cmdline := NewCmdLine(stdin)

		go io.Copy(cmdline, s)
		go io.Copy(s, stdout)
		go io.Copy(s, stderr)

		if err := cmd.Run(); err != nil {
			log.Println(err)
		}

		cmd.Wait()
	})

	log.Println("starting ssh server on port ...")
	log.Fatal(ssh.ListenAndServe(fmt.Sprintf("127.0.0.1:%d", port), nil))
}
