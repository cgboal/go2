package lib

import (
	"net"
	"io"
	"log"
	"golang.org/x/crypto/ssh"
	"fmt"
)

func ConnectToHost(host string, port string) {
	connectString := fmt.Sprintf("%s:%s", host, port)
	sshConfig := &ssh.ClientConfig{
	}

	sshConfig.HostKeyCallback = ssh.InsecureIgnoreHostKey()

	serverConn, err := ssh.Dial("tcp", connectString, sshConfig)
	if err != nil {
		log.Fatal(err)
	}

	listener, err := serverConn.Listen("tcp", "localhost:22222")
	if err != nil {
		log.Fatal("Error listening on remote port")
	}
	defer listener.Close()

	local, err := net.Dial("tcp", "localhost:1337")
	if err != nil {
		log.Fatal("error connecting to local endpoint")
	}

	client, err := listener.Accept()
	if err != nil {
		log.Fatal("Error accepting connection from listener")
	}

	HandleClient(client, local)

}


func HandleClient(client net.Conn, remote net.Conn) {
	defer client.Close()
	chDone := make(chan bool)

	go func() {
		_, err := io.Copy(client, remote)
		if err != nil {
			log.Println(err)
		}
		chDone <- true
	} ()

	go func () {
		_, err := io.Copy(remote, client)
		if err != nil {
			log.Println(err)
		}
		chDone <- true
	} ()

	<-chDone

}
