package main

import (
	"io"
	"log"
	"flag"
	"github.com/gliderlabs/ssh"
	"fmt"
)

func main() {
	port := flag.String("p", "2222", "Port to listen on")
	flag.Parse()

	log.Printf("Starting ssh server on port %s...", *port)

	forwardHandler := &ssh.ForwardedTCPHandler{}
	server := ssh.Server{
		Addr: fmt.Sprintf(":%s", *port),
		Handler: ssh.Handler(func(s ssh.Session) {
			io.WriteString(s, "Remote forwarding available...\n")
			select {}
		}),
		ReversePortForwardingCallback: ssh.ReversePortForwardingCallback(func(ctx ssh.Context, host string, port uint32) bool {
			log.Println("attempt to bind", host, port, "granted")
			return true
		}),
		RequestHandlers: map[string]ssh.RequestHandler{
			"tcpip-forward":        forwardHandler.HandleSSHRequest,
			"cancel-tcpip-forward": forwardHandler.HandleSSHRequest,
		},
	}

	log.Fatal(server.ListenAndServe())
}
