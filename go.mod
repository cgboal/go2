module gitlab.com/cgboal/go2

go 1.13

require (
	github.com/anmitsu/go-shlex v0.0.0-20161002113705-648efa622239 // indirect
	github.com/creack/pty v1.1.7
	github.com/gliderlabs/ssh v0.2.2
	github.com/kr/pty v1.1.8
	golang.org/x/crypto v0.0.0-20200210222208-86ce3cb69678
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3
)
